﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace SignalRHomework.Hubs
{
    public class DrawingHub : Hub
    {
        public async Task SendCoord(int x, int y)
        {
            //kazkas23
            //kazkas1
            //edgaras keicia koda1
            await Clients.Others.SendAsync("receiveCoord", x, y);
        }
    }
}